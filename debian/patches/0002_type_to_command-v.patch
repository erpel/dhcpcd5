From 8b25cff6dbad0c608ab7a936ca368fa1aa637a5b Mon Sep 17 00:00:00 2001
From: Roy Marples <roy@marples.name>
Date: Thu, 24 Feb 2022 12:53:02 +0000
Subject: [PATCH] hooks: Use command -v rather than type

command -v is now required by POSIX, type is technically an extension.
---
 hooks/10-wpa_supplicant   |  4 ++--
 hooks/20-resolv.conf      |  2 +-
 hooks/29-lookup-hostname  |  6 +++---
 hooks/30-hostname.in      |  4 ++--
 hooks/50-ntp.conf         |  2 +-
 hooks/dhcpcd-run-hooks.in | 10 +++++-----
 6 files changed, 14 insertions(+), 14 deletions(-)

diff --git a/hooks/10-wpa_supplicant b/hooks/10-wpa_supplicant
index 04acce08..1da0108d 100644
--- a/hooks/10-wpa_supplicant
+++ b/hooks/10-wpa_supplicant
@@ -102,8 +102,8 @@ wpa_supplicant_stop()
 }
 
 if [ "$ifwireless" = "1" ] && \
-    type wpa_supplicant >/dev/null 2>&1 && \
-    type wpa_cli >/dev/null 2>&1
+    command -v wpa_supplicant >/dev/null 2>&1 && \
+    command -v wpa_cli >/dev/null 2>&1
 then
 	case "$reason" in
 	PREINIT)	wpa_supplicant_start;;
diff --git a/hooks/20-resolv.conf b/hooks/20-resolv.conf
index 504a6c53..7c29e276 100644
--- a/hooks/20-resolv.conf
+++ b/hooks/20-resolv.conf
@@ -11,7 +11,7 @@ nocarrier_roaming_dir="$state_dir/roaming"
 NL="
 "
 : ${resolvconf:=resolvconf}
-if type "$resolvconf" >/dev/null 2>&1; then
+if command -v "$resolvconf" >/dev/null 2>&1; then
 	have_resolvconf=true
 else
 	have_resolvconf=false
diff --git a/hooks/29-lookup-hostname b/hooks/29-lookup-hostname
index 811c7a9c..a1540dd9 100644
--- a/hooks/29-lookup-hostname
+++ b/hooks/29-lookup-hostname
@@ -4,20 +4,20 @@ lookup_hostname()
 {
 	[ -z "$new_ip_address" ] && return 1
 	# Silly ISC programs love to send error text to stdout
-	if type dig >/dev/null 2>&1; then
+	if command -v dig >/dev/null 2>&1; then
 		h=$(dig +short -x $new_ip_address)
 		if [ $? = 0 ]; then
 			echo "$h" | sed 's/\.$//'
 			return 0
 		fi
-	elif type host >/dev/null 2>&1; then
+	elif command -v host >/dev/null 2>&1; then
 		h=$(host $new_ip_address)
 		if [ $? = 0 ]; then 
 			echo "$h" \
 			| sed 's/.* domain name pointer \(.*\)./\1/'
 			return 0
 		fi
-	elif type getent >/dev/null 2>&1; then
+	elif command -v getent >/dev/null 2>&1; then
 		h=$(getent hosts $new_ip_address)
 		if [ $? = 0 ]; then
 			echo "$h" | sed 's/[^ ]* *\([^ ]*\).*/\1/'
diff --git a/hooks/30-hostname.in b/hooks/30-hostname.in
index abeb3696..b0a37576 100644
--- a/hooks/30-hostname.in
+++ b/hooks/30-hostname.in
@@ -25,7 +25,7 @@ _hostname()
 	if [ -z "${1+x}" ]; then
 		if [ -r /proc/sys/kernel/hostname ]; then
 			read name </proc/sys/kernel/hostname && echo "$name"
-		elif type hostname >/dev/null 2>/dev/null; then
+		elif command -v hostname >/dev/null 2>/dev/null; then
 			hostname
 		elif sysctl kern.hostname >/dev/null 2>&1; then
 			sysctl -n kern.hostname
@@ -39,7 +39,7 @@ _hostname()
 
 	if [ -w /proc/sys/kernel/hostname ]; then
 		echo "$1" >/proc/sys/kernel/hostname
-	elif [ -n "$1" ] && type hostname >/dev/null 2>&1; then
+	elif [ -n "$1" ] && command -v hostname >/dev/null 2>&1; then
 		hostname "$1"
 	elif sysctl kern.hostname >/dev/null 2>&1; then
 		sysctl -w "kern.hostname=$1" >/dev/null
diff --git a/hooks/50-ntp.conf b/hooks/50-ntp.conf
index 046ab6b3..767e8360 100644
--- a/hooks/50-ntp.conf
+++ b/hooks/50-ntp.conf
@@ -43,7 +43,7 @@ fi
 
 # Debian has a separate file for DHCP config to avoid stamping on
 # the master.
-if [ "$ntp_service" = ntpd ] && type invoke-rc.d >/dev/null 2>&1; then
+if [ "$ntp_service" = ntpd ] && command -v invoke-rc.d >/dev/null 2>&1; then
 	[ -e /var/lib/ntp ] || mkdir /var/lib/ntp
 	: ${ntp_service:=ntp}
 	: ${NTP_DHCP_CONF:=/var/lib/ntp/ntp.conf.dhcp}
diff --git a/hooks/dhcpcd-run-hooks.in b/hooks/dhcpcd-run-hooks.in
index a237f6af..c858da3b 100644
--- a/hooks/dhcpcd-run-hooks.in
+++ b/hooks/dhcpcd-run-hooks.in
@@ -67,7 +67,7 @@ key_get_value()
 	key="$1"
 	shift
 
-	if type sed >/dev/null 2>&1; then
+	if command -v sed >/dev/null 2>&1; then
 		sed -n "s/^$key//p" $@
 	else
 		for x do
@@ -89,7 +89,7 @@ remove_markers()
 	in_marker=0
 
 	shift; shift
-	if type sed >/dev/null 2>&1; then
+	if command -v sed >/dev/null 2>&1; then
 		sed "/^$m1/,/^$m2/d" $@
 	else
 		for x do
@@ -109,9 +109,9 @@ comp_file()
 {
 	[ -e "$1" ] && [ -e "$2" ] || return 1
 
-	if type cmp >/dev/null 2>&1; then
+	if command -v cmp >/dev/null 2>&1; then
 		cmp -s "$1" "$2"
-	elif type diff >/dev/null 2>&1; then
+	elif command -v diff >/dev/null 2>&1; then
 		diff -q "$1" "$2" >/dev/null
 	else
 		# Hopefully we're only working on small text files ...
@@ -178,7 +178,7 @@ syslog()
 	err|error)	echo "$interface: $*" >&2;;
 	*)		echo "$interface: $*";;
 	esac
-	if type logger >/dev/null 2>&1; then
+	if command -v logger >/dev/null 2>&1; then
 		logger -i -p daemon."$lvl" -t dhcpcd-run-hooks "$interface: $*"
 	fi
 }
